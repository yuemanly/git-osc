---
layout: post
title: "美国知名媒体 TechCrunch 关注中国建设独立开源平台"
---

<p><span style="color:null">近日，美国知名科技媒体 TechCrunch 发表了一篇题为&ldquo;</span><a href="https://techcrunch.com/2020/08/21/china-is-building-its-github-alternative-gitee/" target="_blank"><span style="color:null">中国正在建立一个名为 Gitee 的 GitHub 替代方案</span></a><span style="color:null">（China is building a GitHub alternative called Gitee）&rdquo;的报道，阐述了中美技术脱钩的影响进一步延伸到代码托管领域。</span></p>

<p><img height="817" src="https://static.oschina.net/uploads/space/2020/0822/090618_dapB_3820517.png" width="1364" /></p>

<p><span style="color:null">文章指出，中国开发者非常依赖 GitHub，而现在，中国正在警惕政治冲突可能会给 GitHub 在国内的正常使用带来影响。因为在去年 7 月，已经被微软收购的 GitHub 切断了包括伊朗、叙利亚和克里米亚在内的被美国制裁的国家用户使用其某些服务的权限，引起了全球开发者社区的愤怒和恐慌。</span></p>

<p><span style="color:null">因此，该文作者认为中国本土的代码托管平台 Gitee 将很有可能成为 GitHub 在中国国内的替代品，有评论认为这是出于政府意志。</span></p>

<p><span style="color:null">该文应当是针对前些天</span><a href="https://www.oschina.net/news/117955/gitee-news" target="_blank"><span style="color:null">《工信部携码云 Gitee 入场，国内开源生态建设进入快车道》</span></a><span style="color:null">一文的发挥。实际上工信部展现出了非常开放的心态，没有选择以政府身份自建源码托管平台，而是信任和支持 Gitee。</span></p>

<p><span style="color:null">对此你怎么看？</span></p>
