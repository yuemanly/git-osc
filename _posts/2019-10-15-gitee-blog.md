---
layout: post
title: "码云 Gitee 组织增加“关注” —— 欢迎国内开源组织入驻"
---

<p>在码云 Gitee 上，你可以关注某个仓库，关注某个开发者。继上周&nbsp;<a href="https://www.oschina.net/news/110448/gitee-organization-update">码云 Gitee 的组织改版</a>&nbsp;之后，我们又为您增加了&ldquo;关注组织&rdquo; 的特性。</p>

<p>以腾讯 Tars 组织为例&nbsp;<a href="https://gitee.com/tarscloud">https://gitee.com/tarscloud</a>&nbsp;，你可以通过组织页面右侧的&ldquo;关注&rdquo;按钮来关注该组织。</p>

<p><img src="https://static.oschina.net/uploads/space/2019/1015/071526_ntQh_12.png" width="150" /></p>

<p>关注的组织将出现在你的关注列表中，组织相关的最新动态将出现在你的个人工作台上。</p>

<p><strong>码云 Gitee 开源组织：</strong></p>

<ul>
	<li>腾讯蓝鲸智云：<a href="https://gitee.com/Tencent-BlueKing" target="_blank">https://gitee.com/Tencent-BlueKing</a></li>
	<li>滴滴开源：<a href="https://gitee.com/didiopensource" target="_blank">https://gitee.com/didiopensource</a></li>
	<li>华为鸿蒙：<a href="https://gitee.com/harmonyos" target="_blank">https://gitee.com/harmonyos</a></li>
	<li>华为欧拉&nbsp;<a href="https://gitee.com/openeuler">https://gitee.com/openeuler</a></li>
	<li>北京大学数字视频编解码技术国家工程实验室视频编码组&nbsp;<a href="https://gitee.com/pkuvcl">https://gitee.com/pkuvcl</a></li>
	<li>腾讯微信读书开发团队&nbsp;<a href="https://gitee.com/QMUI">https://gitee.com/QMUI</a></li>
	<li>蚂蚁金服 SOFASTACK：<a href="https://gitee.com/sofastack" target="_blank">https://gitee.com/sofastack</a></li>
	<li>腾讯TARS：<a href="https://gitee.com/tarscloud" target="_blank">https://gitee.com/tarscloud</a></li>
	<li>WeBank微众银行：<a href="https://gitee.com/webank" target="_blank">https://gitee.com/webank</a></li>
	<li>运满满：<a href="https://gitee.com/ymm-tech">https://gitee.com/ymm-tech</a></li>
	<li>北京视图更新科技：&nbsp;<a href="https://gitee.com/view-design">https://gitee.com/view-design</a></li>
	<li>FibJS&nbsp;<a href="https://gitee.com/fibjs">https://gitee.com/fibjs</a></li>
</ul>

<p>欢迎更多开源组织入驻<a href="http://gitee.com/" target="_blank">码云 Gitee</a>&nbsp;，与众多开发者一同完善开源项目，分享开源成果。</p>
