---
layout: post
title: "码云联合实训邦推出高校软件工程视频实训课程"
---

<p>随着软件工程类课程在各大高校如火如荼地开展，如何有效地系统化地开展教学成为这类课程任课教师的关注问题。</p>

<ul>
	<li>目前大多数教师都是采用通过FTP、课程网站等方式布置作业，批改过程繁琐</li>
</ul>

<ul>
	<li>部分以项目的形式布置的作业，教师难以评定小组成员的实际贡献</li>
</ul>

<ul>
	<li>学生组成的作业项目小组开发模式随意，没有对整个项目团队开发进度的清晰认识</li>
</ul>

<ul>
	<li>学生与学生，教师与学生之间的互动较少</li>
</ul>

<p>码云推出的<a href="https://gitee.com/education" target="_blank"><u>高校版</u></a>有效地解决了以上问题，优化教育过程，并提高教学效果。为了让教师和学生熟练使用高校版，码云联合<a href="http://www.sxbang.net/" target="_blank"><u>实训邦</u></a><strong>推出了</strong><a href="http://www.sxbang.net/projects/121?origin=gitee" target="_blank"><strong><u>码云高校版实训教程</u></strong></a>，旨在助力计算机专业教学改革，软件工程实践落地。</p>

<p>此课程以团队协作为故事背景，从3个案例故事带学生一步步学习Git基本命令，做能独立使用Git管理个人文件版本；能团队协作使用Git合作开发；能师生使用高校版有效管理团队成员，作业与任务，计划与监控等。</p>

<p><img src="https://static.oschina.net/uploads/space/2019/0311/075632_Ka4j_4062684.png" /></p>

<p>观看完整版码云高校版实训课程：<a href="http://www.sxbang.net/projects/121?origin=gitee" target="_blank">http://www.sxbang.net/projects/121</a></p>
