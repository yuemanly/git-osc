---
layout: post
title: "码云Gitee对接华为云ContainerOps实现 DevOps 自动化"
---

<p><img alt="" height="300" src="https://static.oschina.net/uploads/space/2019/0820/075839_3zFu_12.jpg" width="300" /></p>

<p>日前，码云 Gitee 已经完成了和华为云 ContainerOps 服务的对接，利用华为云平台赋能码云 Gitee 的 DevOps 能力。</p>

<p>你可以通过仓库的 DevOps 菜单进入 ContainerOps 服务，如下图所示：</p>

<p><img alt="" height="309" src="https://oscimg.oschina.net/oscnet/bb2ffdff3b240fbca7e378036c92e8c3552.jpg" width="800" /></p>

<p>目前该服务还处于公测阶段，华为云为所有码云的开发者免费提供 50 次的流水线执行，更多的配额可通过华为云平台购买。</p>

<p>详细的使用手册请看&nbsp;<a href="https://gitee.com/help/articles/4264">https://gitee.com/help/articles/4264</a></p>

<p>----------</p>

<p>华为云容器交付流水线（ContainerOps）以DevOps理念为基础，面向从源代码到生产上线全流程，提供代码编译、镜像构建、灰度发布、容器化部署等一系列服务，助力企业落地容器DevOps最佳实践。ContainerOps解决了容器化场景下从源码到镜像构建、镜像部署过程中的问题，进一步提升了研发、部署和运维效率。</p>

<p><strong>图1&nbsp;</strong>容器交付流水线&nbsp;<br />
<img src="https://support.huaweicloud.com/productdesc-containerops/zh-cn_image_0173557569.png" /></p>

<h4>ContainerOps 产品功能</h4>

<p><strong>提供容器化场景下的全流程CI/CD能力</strong></p>

<p>ContainerOps提供容器化场景下的全流程CI/CD（持续集成/持续发布）能力，通过使用系统内置流水线或自定义流水线实现从源码到镜像构建、镜像部署的容器化流水线能力。</p>

<p><strong>支持对接源码托管网站构建镜像</strong></p>

<p>ContainerOps支持对接DevCloud、<strong>Gitee</strong>、GitHub、GitLab构建容器镜像，ContainerOps构建出的镜像存储在容器镜像服务中，可使用存储的镜像进行镜像部署。</p>

<p><strong>提供灰度发布模式</strong></p>

<p>ContainerOps提供灰度发布模式，您在正式升级现网版本之前，可以对业务集群的部分实例进行升级、验证，验证通过后再正式发布。</p>

<p><strong>图2&nbsp;</strong>灰度发布&nbsp;<br />
<img src="https://support.huaweicloud.com/productdesc-containerops/zh-cn_image_0169021842.png" /></p>

<p><strong>支持alpha-beta-gamma端到端敏捷交付</strong></p>

<p>ContainerOps支持alpha-beta-gamma端到端敏捷交付，支持一套软件版本使用不同的配置，自动发布到alpha-beta-gamma环境中。每一条流水线独立管理一个（微）服务的生命周期，减少了跨（微）服务、跨DC/Region的操作耦合。</p>

<p><strong>图3&nbsp;</strong>alpha-beta-gamma端到端敏捷交付&nbsp;<br />
<img src="https://support.huaweicloud.com/productdesc-containerops/zh-cn_image_0169021848.png" /></p>

<p><strong>支持对接企业研发平台</strong></p>

<p>ContainerOps提供全功能API，企业可自行对接内部研发平台，统一研发流程的管理入口，确保企业已有研发及流程操作习惯不变，复用已有资源，节约建设成本。</p>

<p><strong>图4&nbsp;</strong>对接企业研发平台&nbsp;<br />
<img src="https://support.huaweicloud.com/productdesc-containerops/zh-cn_image_0169021851.png" /></p>

<p>更多关于华为云 ContainerOps 的介绍请看<a href="https://support.huaweicloud.com/productdesc-containerops/ops_productdesc_0001.html">这里</a>。</p>
