---
layout: post
title: "Gitee 已支持 OSI 认证的第二版木兰开源许可 MulanPSL-2.0"
---

<p>在<a href="https://license.coscl.org.cn/MulanPSL2/">木兰宽松许可证第 2 版</a>（MulanPSL-2.0）通过开源促进会（OSI）认证，成为一个国际化开源许可之后，<a href="https://gitee.com/">Gitee</a> 平台目前已经新增了对该版本许可的支持。</p>

<p><img height="539" src="https://oscimg.oschina.net/oscnet/up-02e8c41010548fa3780e05790aca5df3c0c.png" width="700" /></p>

<p>此前 Gitee 已经支持 MulanPSL-1.0，此次新增支持的 MulanPSL-2.0 在 MulanPSL-1.0&nbsp;的基础上明确了许可证规范语言。开发者可以通过&ldquo;许可证向导&rdquo;轻松选用该许可。</p>

<p><img height="401" src="https://oscimg.oschina.net/oscnet/up-76e9157a10000befd145c024f5aef288e1f.png" width="700" /></p>

<p><img height="422" src="https://oscimg.oschina.net/oscnet/up-82d7ea677b69a89b0c7615c3bb73cebec36.png" width="700" /></p>

<p>2 月 12 日，中国开源云联盟宣布，木兰宽松许可证第 2 版经过严格审批，正式<a href="https://mp.weixin.qq.com/s?__biz=MzI4MDI3MTg5MA==&amp;mid=2247485357&amp;idx=1&amp;sn=dbd07174a6b019cd385bfe1c57d65e52">通过 OSI 认证</a>，被正式批准为国际类别开源许可证（Internationallicenses）。OSI 表示&ldquo;中文版的开源许可证可以鼓励广大中国社区积极参与开源，同时也是对已批准开源许可证列表的宝贵补充&rdquo;。</p>

<p>通过 OSI 认证意味着 MulanPSL-2.0 正式具有国际通用性，可被任一国际开源基金会或开源社区支持采用，并为任一开源项目提供服务。同时，木兰宽松许可证是首个由中国开源产业界联合编制并通过 OSI 认证的开源软件许可证，也标志着我国开源界立足中国贡献全球方面取得突破性进展。</p>
